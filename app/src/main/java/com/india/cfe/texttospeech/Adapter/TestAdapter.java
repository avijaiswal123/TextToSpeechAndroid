package com.india.cfe.texttospeech.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.india.cfe.texttospeech.Activity.TestActivity;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.ModelClass.UserTestsResponse;
import com.india.cfe.texttospeech.R;

import java.util.List;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {
    private Context mcontext;
    private List<UserTestsResponse> testsResponses;
    public TestAdapter(List<UserTestsResponse> testResponse, Context context) {
        this.testsResponses=testResponse;
        this.mcontext=context;


    }

    @NonNull
    @Override
    public TestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.test_adapter,null));
    }

    @Override
    public void onBindViewHolder(@NonNull TestAdapter.ViewHolder holder, final int position) {
        holder.testName_tv.setText(testsResponses.get(position).getTestName());
        holder.testCategory_tv.setText(testsResponses.get(position).getCategory());
        holder.testDifficulty_tv.setText("Difficuly Level "+"||"+testsResponses.get(position).getDifficultylevel());
        holder.totalQuest_tv.setText("Total questions= "+testsResponses.get(position).getTotalQuestion());
        holder.duration_tv.setText("Total Duration "+testsResponses.get(position).getDuration()+"minutes");
        holder.proceedTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, TestActivity.class);
                intent.putExtra(AppConstant.TestType,AppConstant.Real);
                intent.putExtra(AppConstant.Duration,testsResponses.get(position).getDuration());
                intent.putExtra(AppConstant.Category,testsResponses.get(position).getCategory());
                intent.putExtra(AppConstant.Id,testsResponses.get(position).get_id());
                intent.putExtra(AppConstant.TestName,testsResponses.get(position).getTestName());
                intent.putExtra(AppConstant.QuestionIdList,testsResponses.get(position).getQuestions());
                mcontext.startActivity(intent);

                }
        });

        }





    @Override
    public int getItemCount() {
        return testsResponses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView testCategory_tv,testName_tv,testDifficulty_tv,totalQuest_tv,duration_tv;
        TextView proceedTV;
        public ViewHolder(View itemView) {
            super(itemView);
            testCategory_tv=itemView.findViewById(R.id.test_category);
            testName_tv=itemView.findViewById(R.id.test_name);
            testDifficulty_tv=itemView.findViewById(R.id.test_difficulty);
            totalQuest_tv=itemView.findViewById(R.id.total_quest);
            proceedTV=itemView.findViewById(R.id.proceed_tv);
            duration_tv=itemView.findViewById(R.id.duration);

        }
    }
}
