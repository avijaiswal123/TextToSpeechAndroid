package com.india.cfe.texttospeech.Activity;

import android.content.Intent;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.SharedPref;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    private TextToSpeech tts;
    private int MY_DATA_CHECK_CODE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        //This method is to check user has data necessary for TTS to function
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);


    }

    private void forwardToNextActivity() {
        if(SharedPref.getBooleanInSharedPref(AppConstant.IsWelcomed,this)){
            if(SharedPref.getBooleanInSharedPref(AppConstant.IsLogined,this)){
                Intent intent=new Intent(SplashActivity.this,HomeActivity.class);
                startActivity(intent);
            }else {
                Intent intent=new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);

            }
        }else {
            Intent intent=new Intent(SplashActivity.this,IntroductionActivity.class);
            startActivity(intent);

        }

    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            if(tts.isLanguageAvailable(Locale.US)==TextToSpeech.LANG_AVAILABLE)
                tts.setLanguage(Locale.US);
//Handler is used because Text to speech take some time for initialization.
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    forwardToNextActivity();
                }
            },4000);

        }
        else if (i == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }


    }

    //If the data is not present, the app will prompt the user to install it.
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                tts = new TextToSpeech(this, this);



            }
            else {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }
}
