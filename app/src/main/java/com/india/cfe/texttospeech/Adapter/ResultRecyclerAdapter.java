package com.india.cfe.texttospeech.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.india.cfe.texttospeech.Activity.ResultActivity;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResult;
import com.india.cfe.texttospeech.R;

import java.util.ArrayList;
import java.util.List;

public class ResultRecyclerAdapter extends RecyclerView.Adapter<ResultRecyclerAdapter.ViewHolder>{
    private List<CompletedTestResult> testResult;
    private Context mContext;
    private ArrayList<String>userAnswersList;



    public ResultRecyclerAdapter(List<CompletedTestResult> testResult, ArrayList<String> userAnswersList, Context context) {
      this.testResult=testResult;
      this.mContext=context;
      this.userAnswersList=userAnswersList;
    }

    @NonNull
    @Override
    public ResultRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_recycler_adapter,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ResultRecyclerAdapter.ViewHolder holder, int position) {
        holder.questionsTV.setText("Q" + (position + 1) + ":-" + testResult.get(position).getText());
        holder.option_aTV.setText(testResult.get(position).getOptions().get(0));
        holder.option_bTV.setText(testResult.get(position).getOptions().get(1));
        holder.option_cTV.setText(testResult.get(position).getOptions().get(2));
        holder.option_dTV.setText(testResult.get(position).getOptions().get(3));

        if (testResult.get(position).getAnswer().trim().equals("1")) {
            holder.answer_tv.setText("Answer : " + holder.option_aTV.getText().toString());
        } else if (testResult.get(position).getAnswer().trim().equals("2")) {
            holder.answer_tv.setText("Answer : " + holder.option_bTV.getText().toString());
        } else if (testResult.get(position).getAnswer().trim().equals("3")) {
            holder.answer_tv.setText("Answer : " + holder.option_cTV.getText().toString());
        } else if (testResult.get(position).getAnswer().trim().equals("4")) {
            holder.answer_tv.setText("Answer : " + holder.option_dTV.getText().toString());
        }


        if (userAnswersList.get(position).equals(AppConstant.NoData)) {
            holder.userAns_tv.setText("Your Answer :  " + AppConstant.NotAswered);

        } else {
            holder.userAns_tv.setText("Your Answer :  " + userAnswersList.get(position));

        }
           String answer=holder.answer_tv.getText().toString().split(":",2)[1].trim();
           String userAnswer=holder.userAns_tv.getText().toString().split(":",2)[1].trim();

           if(userAnswer.equalsIgnoreCase("NotAnswered")){
              holder.userAns_tv.setTextColor(mContext.getResources().getColor(R.color.fragment_color));
            }
            else if (userAnswer.equalsIgnoreCase(answer)) {
                holder.userAns_tv.setTextColor(mContext.getResources().getColor(android.R.color.holo_green_dark));

            } else holder.userAns_tv.setTextColor(Color.RED);

    }


    @Override
    public int getItemCount() {
        return testResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView questionsTV,option_aTV,option_bTV,option_cTV,option_dTV,answer_tv,userAns_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            answer_tv=itemView.findViewById(R.id.answer_tv);
            questionsTV=itemView.findViewById(R.id.quest_tv);
            option_aTV=itemView.findViewById(R.id.option_a_tv);
            option_bTV=itemView.findViewById(R.id.option_b_tv);
            option_cTV=itemView.findViewById(R.id.option_c_tv);
            option_dTV=itemView.findViewById(R.id.option_d_tv);
            userAns_tv=itemView.findViewById(R.id.user_answer_tv);
        }

    }

}
