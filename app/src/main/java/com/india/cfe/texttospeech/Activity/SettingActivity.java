package com.india.cfe.texttospeech.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.SharedPref;
import com.india.cfe.texttospeech.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SettingActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    private AppCompatSpinner dropDownSpinner;
    private LinearLayout speedLayout,volumeLayout,pitchLayout;
    private TextToSpeech textToSpeech;
    private ImageView profileImage;
    private Button updateBTN,defaultBTN,cancelBtn;
    private AppCompatSeekBar seekBarVolume,seekbarSpeed,seekbarPitch;
    private TextView progressTV,settNameTV;
    private Float speedLevel=(float)0.9;
    private Float pitchLevel=(float)1.0;
    private LinearLayout parentLayout;
    private RelativeLayout dialogLayout;
    private AudioManager audioManager = null;
    private Uri uri;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private CollapsingToolbarLayout collapsingLayout;
    private int currentVolumeLevel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initViews();



        textToSpeech=new TextToSpeech(this,this);
        textToSpeech.setSpeechRate(SharedPref.getFloatInSharedPref(AppConstant.Speed,this));
        textToSpeech.setPitch(SharedPref.getFloatInSharedPref(AppConstant.Pitch,this));
        (this).setVolumeControlStream(AudioManager.STREAM_MUSIC);
        audioManager= (AudioManager) (this).getSystemService(Context.AUDIO_SERVICE);
        assert audioManager != null;

        setProfileView();
        setSeekBars();


        speedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(AppConstant.Speed);


            }
        });
        volumeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(AppConstant.Volume);

            }
        });
        pitchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(AppConstant.Pitch);

            }
        });
        updateBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
                SharedPref.saveFloatInSharedPref(AppConstant.Speed,speedLevel,SettingActivity.this);
                SharedPref.saveFloatInSharedPref(AppConstant.Pitch,pitchLevel,SettingActivity.this);


            }
        });
        defaultBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultValue(textToSpeech);
                Snackbar snackbar=Snackbar.make(parentLayout,"Default value for speech settings saved",Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(settNameTV.getText().toString().trim().equals(AppConstant.Volume)){
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,currentVolumeLevel,0);
                }
                dismissDialog();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptionDialog();
            }
        });
        seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        i, 0);
                progressTV.setText(""+i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speekLine(textToSpeech);            }
        });
        seekbarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progressTV.setText(""+i);
                speedLevel=getConvertedFloat(i);
                textToSpeech.setSpeechRate(speedLevel);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speekLine(textToSpeech);            }
        });
        seekbarPitch.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progressTV.setText(""+i);
                pitchLevel=getConvertedFloat(i);
                textToSpeech.setPitch(pitchLevel);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speekLine(textToSpeech);
            }
        });

        setDropDownTestType();

    }

    private void setSeekBars() {
        seekbarPitch.setMax(10);
        seekbarSpeed.setMax(10);
        seekbarPitch.setThumbOffset(5);
        seekbarSpeed.setThumbOffset(5);
        seekBarVolume.setThumbOffset(5);
        seekBarVolume.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        currentVolumeLevel=audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        seekBarVolume.setProgress(currentVolumeLevel);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissDialog();
    }

    private void setDropDownTestType() {
        ArrayList<String> testType=new ArrayList<>();
        testType.add("Text to Speech");
        testType.add("Normal test");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, testType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropDownSpinner.setAdapter(adapter);

//        if(!SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS,this)){
//            dropDownSpinner.setSelection(1);
//        }else {
//            dropDownSpinner.setSelection(0);
//        }




        dropDownSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {

                    case 0:
                        setTextToSpeechMode();
                        break;
                    case 1:
                        setNormalMode();
                        break;
                }}

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setNormalMode() {
        SharedPref.saveBooleanInSharedPref(AppConstant.TestTypeTTS,false,this);
        Snackbar snackbars=  Snackbar.make(parentLayout,"Test Type NORMAL MODE Selected",Snackbar.LENGTH_LONG);
        snackbars.show();
        volumeLayout.setVisibility(View.GONE);
        speedLayout.setVisibility(View.GONE);
        pitchLayout.setVisibility(View.GONE);
        defaultBTN.setVisibility(View.GONE);
    }

    private void setTextToSpeechMode() {
        SharedPref.saveBooleanInSharedPref(AppConstant.TestTypeTTS,true,this);
        Snackbar snackbar= Snackbar.make(parentLayout,"Test Type SPEECH MODE Selected",Snackbar.LENGTH_LONG);
        snackbar.show();
        volumeLayout.setVisibility(View.VISIBLE);
        speedLayout.setVisibility(View.VISIBLE);
        pitchLayout.setVisibility(View.VISIBLE);
        defaultBTN.setVisibility(View.VISIBLE);
    }

    private void setProfileView() {
       // ViewCompat.setTransitionName(findViewById(R.id.app_bar),);

        collapsingLayout.setTitle("Hello  "+SharedPref.getStringFromSharedPref(AppConstant.UserName,this));
        collapsingLayout.setCollapsedTitleTextColor(Color.WHITE);
        String encodedImage=SharedPref.getStringFromSharedPref(AppConstant.EncodedImage,this);
        if(!encodedImage.equals("0")) {
            byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
            profileImage.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

        }

    }
    private void dismissDialog() {
        parentLayout.setVisibility(View.VISIBLE);
        dialogLayout.setVisibility(View.GONE);

    }

    private void showDialog(String settingsName) {
        parentLayout.setVisibility(View.GONE);
        dialogLayout.setVisibility(View.VISIBLE);
        settNameTV.setText(settingsName);
        progressTV.setText("");

        if(settingsName.equals(AppConstant.Speed)){
            seekbarSpeed.setVisibility(View.VISIBLE);
            seekbarPitch.setVisibility(View.GONE);
            seekBarVolume.setVisibility(View.GONE);

        }
        if(settingsName.equals(AppConstant.Pitch)){
            seekbarSpeed.setVisibility(View.GONE);
            seekbarPitch.setVisibility(View.VISIBLE);
            seekBarVolume.setVisibility(View.GONE);

        }
        if(settingsName.equals(AppConstant.Volume)){
            seekbarSpeed.setVisibility(View.GONE);
            seekbarPitch.setVisibility(View.GONE);
            seekBarVolume.setVisibility(View.VISIBLE);


        }

    }

    public float getConvertedFloat(int intVal){

        return (float) ((.10f * intVal)+0.5);
    }
    public void defaultValue(TextToSpeech textToSpeech){
        textToSpeech.setSpeechRate((float) 0.9);
        textToSpeech.setPitch(1);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC),0);
        speekLine(textToSpeech);
        seekBarVolume.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
        SharedPref.saveFloatInSharedPref(AppConstant.Speed, (float) 0.9,this);
        SharedPref.saveFloatInSharedPref(AppConstant.Pitch, (float) 1,this);


    }
    public void speekLine(TextToSpeech textToSpeech){
        textToSpeech.speak("Welcome To uListen",TextToSpeech.QUEUE_FLUSH,null);
    }
    private void showOptionDialog() {


        AlertDialog.Builder ab=new AlertDialog.Builder(this);
        ab.setTitle("Want To change Profile Image ?");
        ab.setMessage("Select Image from");
        ab.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,0);

            }
        });

        ab.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent GalIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                GalIntent.setType("image/*");
                startActivityForResult(GalIntent,2);

            }
        });
        ab.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ab.show();


    }

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode ==0  && resultCode == Activity.RESULT_OK &&data!=null) {
        Bundle bundle = data.getExtras();
        Bitmap bitmap = (Bitmap) bundle.get("data");


        assert bitmap != null;
        saveImage(bitmap);

    }
    if (requestCode ==2  && resultCode == Activity.RESULT_OK &&data!=null) {
        Uri uri = data.getData();
        // Bundle  bundle=data.getExtras();
        Bitmap bitmap = null;//
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);


            saveImage(bitmap);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
    private void ImageCropFuction() {
        try {
            Intent CropIntent = new Intent("com.android.camera.action.CROP");

            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 180);
            CropIntent.putExtra("outputY", 180);
            CropIntent.putExtra("aspectX", 3);
            CropIntent.putExtra("aspectY", 4);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent, 1);

        } catch (ActivityNotFoundException e) {


        }
    }

    private void saveImage(Bitmap bitmap) {



        int h=bitmap.getHeight();
        int w=bitmap.getWidth();

        h=h/4;
        w=w/4;

        bitmap=Bitmap.createScaledBitmap(bitmap, w,h , true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        byte[] b = stream.toByteArray();
        String encoded = Base64.encodeToString(b, Base64.DEFAULT);
        SharedPref.saveStringInSharedPref(AppConstant.EncodedImage,encoded,this);

        profileImage.setImageBitmap(bitmap);

    }

    @Override
    protected void onStop() {
        super.onStop();
        textToSpeech.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        textToSpeech.shutdown();
    }

    private void initViews() {

        speedLayout=findViewById(R.id.speed_layout);
        volumeLayout=findViewById(R.id.volume_layout);
        pitchLayout=findViewById(R.id.pitch_layout);
        profileImage=findViewById(R.id.profile_imageview);
        parentLayout=findViewById(R.id.relative_parent_layout);
        dialogLayout= findViewById(R.id.relative_dialog_layout);
        updateBTN=findViewById(R.id.dialog_update);
        cancelBtn=findViewById(R.id.dialog_cancel);
        defaultBTN=findViewById(R.id.default_btn);
        progressTV=findViewById(R.id.progress_tv);
        dropDownSpinner=findViewById(R.id.test_type_name);
        seekBarVolume=findViewById(R.id.seekbar_volume);
        seekbarPitch=findViewById(R.id.seekbar_pitch);
        seekbarSpeed=findViewById(R.id.seekbar_speed);
        settNameTV=findViewById(R.id.sett_name_tv);
        toolbar=findViewById(R.id.toolbar);
        fab=findViewById(R.id.fab);
        collapsingLayout=findViewById(R.id.collapsing_toolbar);

    }

    @Override
    public void onInit(int i) {

    }
}
