package com.india.cfe.texttospeech.Fragments.IntroScreenFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.india.cfe.texttospeech.R;

/**
 * Created by Admin on 7/8/2018.
 */

public class IntroFragment_3 extends Fragment{
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.intro_slide_3,container,false);
    }
}
