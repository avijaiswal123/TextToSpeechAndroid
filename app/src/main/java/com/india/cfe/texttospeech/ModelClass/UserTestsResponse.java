package com.india.cfe.texttospeech.ModelClass;

import java.util.ArrayList;

public class UserTestsResponse {
    private String _id;
    private String testName;
    private String category;
    private float duration;
    private String difficultylevel;
    private String totalQuestion;
    private String tcode = null;
    private boolean practice;
    private ArrayList<String> questions;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public String getDifficultylevel() {
        return difficultylevel;
    }

    public void setDifficultylevel(String difficultylevel) {
        this.difficultylevel = difficultylevel;
    }

    public String getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public String getTcode() {
        return tcode;
    }

    public void setTcode(String tcode) {
        this.tcode = tcode;
    }

    public boolean isPractice() {
        return practice;
    }

    public void setPractice(boolean practice) {
        this.practice = practice;
    }

    public ArrayList<String> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<String> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserTestsResponse{");
        sb.append("_id='").append(_id).append('\'');
        sb.append(", testName='").append(testName).append('\'');
        sb.append(", category='").append(category).append('\'');
        sb.append(", duration=").append(duration);
        sb.append(", difficultylevel='").append(difficultylevel).append('\'');
        sb.append(", totalQuestion='").append(totalQuestion).append('\'');
        sb.append(", tcode='").append(tcode).append('\'');
        sb.append(", practice=").append(practice);
        sb.append(", questions=").append(questions);
        sb.append('}');
        return sb.toString();
    }
}
