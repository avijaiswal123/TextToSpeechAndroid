package com.india.cfe.texttospeech.Fragments.HomeScreenFragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.india.cfe.texttospeech.Adapter.TestAdapter;
import com.india.cfe.texttospeech.Adapter.TestCategoryList;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.ModelClass.LoginResponse;
import com.india.cfe.texttospeech.ModelClass.UserTestsResponse;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.RestApis;
import com.india.cfe.texttospeech.AppUtils.RetrofitMaker;
import com.india.cfe.texttospeech.AppUtils.SharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TestFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private RecyclerView test_recycler;
    private TestAdapter testAdapter;
    private RecyclerView.LayoutManager testLayoutManager;
    private SwipeRefreshLayout swipeLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    private AppCompatSpinner dropdownSpinner;
    private LinearLayout noInternetLayout,noTestLayout;
    public static ArrayList<String> qIDList=new ArrayList<>();
    public static ArrayList<ArrayList<String>> questionsIDList=new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private String category;
    private ArrayList<String> spinnerCategory;
    private ArrayAdapter<String> adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.test_fragment,container,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        test_recycler=view.findViewById(R.id.test_recycler);
        testLayoutManager=new LinearLayoutManager(getContext());
        test_recycler.setLayoutManager(testLayoutManager);
        noInternetLayout=view.findViewById(R.id.no_internet_linear);
        swipeLayout=view.findViewById(R.id.swipe_layout);
        swipeLayout.setOnRefreshListener(this);
        dropdownSpinner=view.findViewById(R.id.spinner_category);
        noTestLayout=view.findViewById(R.id.no_test_linear);
        coordinatorLayout=view.findViewById(R.id.coordinatorLayout);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);

        dropdownSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(checkNetworkConnection()){

                        if(position==1){
                            getTestData(null);
                        category=null;

                        }else if(position>1){

                            category=spinnerCategory.get(position);
                            getTestData(category);

                        }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(checkNetworkConnection()){
            getTestCategoryList();
            getTestData(null);


        }else {
            noTestLayout.setVisibility(View.GONE);
            test_recycler.setVisibility(View.GONE);
            noInternetLayout.setVisibility(View.VISIBLE);
        }



    }

    @Override
    public void onResume() {
        super.onResume();


    }
    private void getTestCategoryList(){
         dropdownSpinner.setVisibility(View.GONE);


        String auth="JWT "+SharedPref.getStringFromSharedPref(AppConstant.Token,getContext());
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        final Call<TestCategoryList> testCategoryResponse=restApis.getTestCategories(auth);
        testCategoryResponse.enqueue(new Callback<TestCategoryList>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(@NonNull Call<TestCategoryList> call, @NonNull Response<TestCategoryList> response) {
                if(response.body() !=null){

                    spinnerCategory = new ArrayList<String>();
                    spinnerCategory.add("Select Category");
                    spinnerCategory.add("All tests");

                    spinnerCategory.addAll(response.body().getType());


                    adapter = new ArrayAdapter<String>((getActivity().getApplicationContext()), android.R.layout.simple_spinner_dropdown_item, spinnerCategory);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dropdownSpinner.setAdapter(adapter);
                    dropdownSpinner.setVisibility(View.VISIBLE);
                    Snackbar snackbar=Snackbar.make(coordinatorLayout,"Test Categories Added ",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<TestCategoryList> call, Throwable t) {
                Snackbar snackbar=Snackbar.make(coordinatorLayout,"Something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        });
        }



    private void getTestData(String category) {
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        noTestLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
        test_recycler.setVisibility(View.GONE);
        String id=SharedPref.getStringFromSharedPref(AppConstant.Id,getContext());
        String auth= "JWT "+SharedPref.getStringFromSharedPref(AppConstant.Token,getContext());

        Retrofit retrofit = new RetrofitMaker().instanceMaker(AppConstant.BaseUrl,id,auth);
        RestApis restApis=retrofit.create(RestApis.class);
        final Call<List<UserTestsResponse>> testResponse = restApis.getUserTestResponse(category);
        testResponse.enqueue(new Callback<List<UserTestsResponse>>() {
            @Override
            public void onResponse(Call<List<UserTestsResponse>> call, Response<List<UserTestsResponse>> response) {
                if(response.body() != null){
                    List<UserTestsResponse> testsResponses=response.body();
                    if(testsResponses !=null && testsResponses.size()>0 ){
                        for(UserTestsResponse testsResponse: testsResponses){
                            qIDList.add(testsResponse.get_id());
                            questionsIDList.add(testsResponse.getQuestions());
                            testAdapter=new TestAdapter(testsResponses,getContext());
                            test_recycler.setAdapter(testAdapter);
                            test_recycler.setVisibility(View.VISIBLE);
                        }}
                        else{
                            noTestLayout.setVisibility(View.VISIBLE);
                        }

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);


                    }

            }

            @Override
            public void onFailure(Call<List<UserTestsResponse>> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                noTestLayout.setVisibility(View.GONE);
                Snackbar snackbar=Snackbar.make(coordinatorLayout,"Something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        });


    }

    @Override
    public void onRefresh() {
        if(checkNetworkConnection()){
            getTestCategoryList();
            getTestData(category);



        }else {
            noTestLayout.setVisibility(View.GONE);
            noInternetLayout.setVisibility(View.VISIBLE);
            test_recycler.setVisibility(View.GONE);
        }
        swipeLayout.setRefreshing(false);

    }
    public boolean checkNetworkConnection() {
        boolean networkStatus = false;
        ConnectivityManager conMgr = (ConnectivityManager) (getContext()).getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        networkStatus = netInfo != null;
        return networkStatus;
    }
}
