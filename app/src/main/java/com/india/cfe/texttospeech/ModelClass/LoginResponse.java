package com.india.cfe.texttospeech.ModelClass;

import java.io.Serializable;

public class LoginResponse implements Serializable{
    private String token;
    private String msg;
    private User user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}





