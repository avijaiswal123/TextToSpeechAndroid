package com.india.cfe.texttospeech.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.india.cfe.texttospeech.Activity.TestActivity;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.R;

public class PracticeTestAdapter extends RecyclerView.Adapter<PracticeTestAdapter.ViewHolder>{
    private String testName[]={"Test1","Test2","Test3","Test4","Test5"};
    private Context mcontext;
    private  float duration= (float) 3.0;
    private int lastPosition=-1;
    public PracticeTestAdapter(Context context) {
        this.mcontext=context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.practice_test_adapter,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.testName_TV.setText(testName[position]+" || Practice");
        holder.duration_tv.setText("Test Duration= 3.0 minutes");
        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(mcontext, R.anim.slide_from_right);
            holder.linearLayout.startAnimation(animation);
            lastPosition = position;
        }
        holder.proceedTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(mcontext, TestActivity.class);
               intent.putExtra(AppConstant.PracTestButtonIndex,position);
               intent.putExtra(AppConstant.TestType,AppConstant.Practice);
               intent.putExtra(AppConstant.Duration,duration);
               mcontext.startActivity(intent);

               }
        });

    }



    @Override
    public int getItemCount() {
        return testName.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView testName_TV;
        TextView proceedTV,duration_tv;
        LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            testName_TV = itemView.findViewById(R.id.test_name_tv);
            proceedTV = itemView.findViewById(R.id.proced_tv);
            duration_tv=itemView.findViewById(R.id.duration_tv);
            linearLayout=itemView.findViewById(R.id.linear_layout);
        }
    }
}
