package com.india.cfe.texttospeech.Activity;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.india.cfe.texttospeech.Adapter.CompletedTestAdapter;
import com.india.cfe.texttospeech.Adapter.ResultRecyclerAdapter;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.RestApis;
import com.india.cfe.texttospeech.AppUtils.SharedPref;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.CompletedFragment;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.TestFragment;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResponse;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResult;
import com.india.cfe.texttospeech.R;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultActivity extends AppCompatActivity {
    private RecyclerView resultRecycler;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;
    private CoordinatorLayout coordinatorLayout;
    private FrameLayout parentLayout;
    private LinearLayout circularOverlayLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    private FloatingActionButton fabBTN;
    public  Boolean isOpen = false;
    private String testID;
    private PieChart pieChart;
    private BarChart barChart;
    private ArrayList<String> userAnswersList;
    private List<CompletedTestResult> testResult;
    private Boolean isLaunchFromTestPage=false;
    private String testName;
    private TextView testNameTV;
    private ArrayList<Integer> resultList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        resultRecycler=findViewById(R.id.result_recycler);
        layoutManager=new LinearLayoutManager(this);
        resultRecycler.setLayoutManager(layoutManager);
        parentLayout=findViewById(R.id.parent_frame_layout);
        circularOverlayLayout=findViewById(R.id.linear_overlayout);
        coordinatorLayout=findViewById(R.id.coordinator_layout);
        refreshLayout=findViewById(R.id.swipe_layout);
        testNameTV=findViewById(R.id.test_name);
        fabBTN=findViewById(R.id.fab_btn);
        fabBTN.setImageResource(R.drawable.ic_view_show_chart_24dp);
        pieChart=findViewById(R.id.piechart);
        barChart=findViewById(R.id.barchart);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        Bundle bundle=getIntent().getExtras();

        if(bundle!=null){
            testID=bundle.getString(AppConstant.TestID);
            userAnswersList=bundle.getStringArrayList(AppConstant.UserAnswersList);
            isLaunchFromTestPage=bundle.getBoolean("launchFromTestPage");
            testName=bundle.getString(AppConstant.TestName);
            testNameTV.setText(testName);
            }

        for(int i=0; i< TestFragment.qIDList.size();i++){
            if(testID.equals(TestFragment.qIDList.get(i))){
                getTestsREsults(TestFragment.questionsIDList.get(i));
            }
        }

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
             //   getTestsREsults(testIDList);
                refreshLayout.setRefreshing(false);
            }
        });


        fabBTN.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                showCircularAnimation();
            }
        });
    }

    private void getTestsREsults(ArrayList<String> testIDList) {
        mShimmerViewContainer.startShimmerAnimation();
        String token="JWT "+ SharedPref.getStringFromSharedPref(AppConstant.Token,this);
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        Call<List<CompletedTestResult>> testResults=restApis.getTestResults(token,testIDList);
        testResults.enqueue(new Callback<List<CompletedTestResult>>() {
            @Override
            public void onResponse(Call<List<CompletedTestResult>> call, Response<List<CompletedTestResult>> response) {

                if(response.body()!=null && response.body().size()>0){
                     testResult=response.body();


                    ResultRecyclerAdapter resultAdapter=new ResultRecyclerAdapter(testResult,userAnswersList,ResultActivity.this);
                    resultRecycler.setAdapter(resultAdapter);
                    setDataOnPieChart();
                }


                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                parentLayout.setVisibility(View.VISIBLE);
                fabBTN.setVisibility(View.VISIBLE);


            }

            @Override
            public void onFailure(Call<List<CompletedTestResult>> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                parentLayout.setVisibility(View.VISIBLE);
                fabBTN.setVisibility(View.GONE);
                Snackbar snackbar=Snackbar.make(coordinatorLayout,"Something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showCircularAnimation(){
        if (!isOpen) {

            int x = parentLayout.getRight();
            int y = parentLayout.getBottom();
            int startRadius = 0;
            Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();  // deprecated
            int height = display.getHeight();  // deprecated
            int endRadius = (int) Math.hypot(width, height);
            Animator anim = ViewAnimationUtils.createCircularReveal(circularOverlayLayout, x, y, startRadius, endRadius);
            anim.setDuration(1000);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    circularOverlayLayout.setVisibility(View.VISIBLE);
                    setDataOnPieChart();
                    fabBTN.setClickable(false);
                }
                @Override
                public void onAnimationEnd(Animator animation) {
                  //  resultRecycler.setVisibility(View.GONE);
                    fabBTN.setImageResource(android.R.drawable.ic_delete);
                    fabBTN.setClickable(true);
                    refreshLayout.setEnabled(false);
                    }
                @Override
                public void onAnimationCancel(Animator animation) {
                    }
                @Override
                public void onAnimationRepeat(Animator animation) {

                    }
            });

            anim.start();
            isOpen = true;

            } else {

            int x = parentLayout.getRight();
            int y = parentLayout.getBottom();
            int startRadius = Math.max(parentLayout.getWidth(), parentLayout.getHeight());
            int endRadius = 0;
            Animator anim = ViewAnimationUtils.createCircularReveal(circularOverlayLayout, x, y, startRadius, endRadius);
            anim.setDuration(1000);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                   resultRecycler.setVisibility(View.VISIBLE);
                   fabBTN.setClickable(false);
                }
                @Override
                public void onAnimationEnd(Animator animator) {
                    circularOverlayLayout.setVisibility(View.GONE);
                    fabBTN.setImageResource(R.drawable.ic_view_show_chart_24dp);
                    fabBTN.setClickable(true);
                }
                @Override
                public void onAnimationCancel(Animator animator) {
                    }
                    @Override
                public void onAnimationRepeat(Animator animator) {
                    }
            });
            anim.start();
            isOpen = false;
        }
    }
    public void setDataOnPieChart(){
        int rightAnswer=0;
        int wrongAnswer=0;
        int notAnswered=0;

        resultList = new ArrayList<>();
        if(userAnswersList!=null && testResult!=null){
           for(int i=0 ;i<testResult.size() ;i++){
               String testAnswer="";
               if(testResult.get(i).getAnswer().trim().equals("1")) {
                   testAnswer=testResult.get(i).getOptions().get(0);
               }else if(testResult.get(i).getAnswer().trim().equals("2")){
                   testAnswer=testResult.get(i).getOptions().get(1);
               }else if(testResult.get(i).getAnswer().trim().equals("3")) {
                   testAnswer=testResult.get(i).getOptions().get(2);
               }else if(testResult.get(i).getAnswer().trim().equals("4")) {
                   testAnswer=testResult.get(i).getOptions().get(3);
               }



               if(userAnswersList.get(i).trim().equals(testAnswer.trim())){
                  rightAnswer++;
              }else if(userAnswersList.get(i).trim().equals(AppConstant.NotAswered)|| userAnswersList.get(i).trim().equals(AppConstant.NoData)){
                   notAnswered++;
               }else wrongAnswer++;

           }

            resultList.add(wrongAnswer);
            resultList.add(rightAnswer);
            resultList.add(notAnswered);




//setting data on pie chart
        String answersType[]={"Wrong","Correct","NotAnswered"};
       // Integer results[]={20,50,30};
        List<PieEntry> pieEntries=new ArrayList<>();
        for(int i=0 ; i<answersType.length ;i++){
            pieEntries.add(new PieEntry(resultList.get(i),answersType[i]));

        }
        PieDataSet pieDataSet=new PieDataSet(pieEntries,"");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData=new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.setDrawEntryLabels(false);
        pieChart.animateY(2000);
        pieChart.invalidate();


//setting data on bar chart
            List<BarEntry> barEntries=new ArrayList<>();
            barEntries.add(new BarEntry(0f,wrongAnswer));
            barEntries.add(new BarEntry(1f,rightAnswer));
            barEntries.add(new BarEntry(2f,notAnswered));


            BarDataSet set = new BarDataSet(barEntries, "BarDataSet");
            set.setColors(ColorTemplate.COLORFUL_COLORS);
            BarData data = new BarData(set);
            data.setBarWidth(0.9f);
            XAxis axis=barChart.getXAxis();
            axis.setEnabled(false);
            barChart.setData(data);
            barChart.animateY(2000);
            barChart.setFitBars(true); // make the x-axis fit exactly all bars
            barChart.invalidate();
            barChart.setPinchZoom(false);
            barChart.setDoubleTapToZoomEnabled(false);






        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {

        if(isOpen){

            showCircularAnimation();

        }else {
            if(isLaunchFromTestPage){
                Intent intent=new Intent(ResultActivity.this, HomeActivity.class);
                SharedPref.saveIntegerInSharedPref(AppConstant.WrongAnswer,resultList.get(0),this);
                SharedPref.saveIntegerInSharedPref(AppConstant.RightAnswer,resultList.get(1),this);
                SharedPref.saveIntegerInSharedPref(AppConstant.NoAnswer,resultList.get(2),this);
//                CompletedFragment completedFragment=new CompletedFragment();
//                Bundle bundle=new Bundle();
//                bundle.putIntegerArrayList(AppConstant.ResultList,resultList);
//                completedFragment.setArguments(bundle);
                startActivity(intent);

            }else  finish();



        }
    }
}
