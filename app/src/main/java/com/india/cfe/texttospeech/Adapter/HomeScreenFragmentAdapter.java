package com.india.cfe.texttospeech.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.CompletedFragment;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.PracticeFragment;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.TestFragment;

public class HomeScreenFragmentAdapter extends FragmentPagerAdapter {

    public HomeScreenFragmentAdapter(FragmentManager fm) {
        super(fm);
       // notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new PracticeFragment();
            case 1:
                return new TestFragment();
            case 2:
                return new CompletedFragment();
//            case 3:
//                return new SettingsFragment();
        }
       return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Practice";
            case 1:
                return "Test";
            case 2:
                return "Completed Tests";

            default:
                return null;

        }
    }

}
