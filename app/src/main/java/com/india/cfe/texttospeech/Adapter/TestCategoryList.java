package com.india.cfe.texttospeech.Adapter;

import java.io.Serializable;
import java.util.ArrayList;

public class TestCategoryList implements Serializable{



    private ArrayList<String> type;
    private ArrayList<String> levels;
    private ArrayList<String> testStates;

    public ArrayList<String> getType() {
        return type;
    }

    public void setType(ArrayList<String> type) {
        this.type = type;
    }

    public ArrayList<String> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<String> levels) {
        this.levels = levels;
    }

    public ArrayList<String> getTestStates() {
        return testStates;
    }

    public void setTestStates(ArrayList<String> testStates) {
        this.testStates = testStates;
    }




}
