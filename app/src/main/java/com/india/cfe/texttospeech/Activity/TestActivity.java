package com.india.cfe.texttospeech.Activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.india.cfe.texttospeech.Adapter.NavigRecyclerAdapter;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.EndDrawerToggle;
import com.india.cfe.texttospeech.ModelClass.QuestAndOptionResponse;
import com.india.cfe.texttospeech.ModelClass.SaveTestRequst;
import com.india.cfe.texttospeech.ModelClass.SaveTestResponse;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.RestApis;
import com.india.cfe.texttospeech.AppUtils.SharedPref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class TestActivity extends AppCompatActivity implements TextToSpeech.OnInitListener,NavigRecyclerAdapter.OnQuestClick {

    private int practiceTestBtnIndex,realTestBtnIndex;
    private TextView questionsTV,option_aTV,option_bTV,option_cTV,option_dTV,answer_tv,questNoTV,timerTV,startTV;
    private TextView instructTV1,instructTV2,instructTV3,instructTV4,instructTV5,instructTV6,savingTextTV;
    private TextView nextTV,repeatTV,previousTV,finishTV;
    private DrawerLayout drawerLayout;
    private CoordinatorLayout coordinatorLayout;
    private android.support.v7.widget.Toolbar toolbar;
    private ArrayList<String> questions;
    private ArrayList<String> options_a;
    private ArrayList<String> options_b;
    private ArrayList<String> options_c;
    private ArrayList<String> options_d;
    private ArrayList<String> correctAnswer;
    private int index=0;
    private TextToSpeech myTTS;
    private LinearLayout instructionLayout,testLayout;
    private String testType;
    private ArrayList<String> questionsIdList;
    private boolean testReady=true;
    private int questSize;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private List<QuestAndOptionResponse> testLists;
    private ArrayList<String> answersList=new ArrayList<>();
    SaveTestRequst saveTestRequst=new SaveTestRequst();
    private ShimmerFrameLayout mShimmerViewContainer;
    private RecyclerView navigRecycler;
    private RecyclerView.LayoutManager layoutManager;
    private int timerDuration;
    private CardView questCardView;
    private Boolean instructionSpoken=true;
    private CountDownTimer countDownTimer;
    private boolean optionSpoken=false;
    private FrameLayout parentLayout;
    private AppCompatSpinner dropDownSpinner;
    private Boolean instructionListened=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        initViews();
        //setSupportActionBar(toolbar);
        EndDrawerToggle toogle=new EndDrawerToggle(this,drawerLayout,toolbar,R.string.close,R.string.open);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();

        layoutManager=new GridLayoutManager(this,4);
        navigRecycler.setLayoutManager(layoutManager);
        setDropDownTestType();




        myTTS=new TextToSpeech(this,this);
        myTTS.setSpeechRate(SharedPref.getFloatInSharedPref(AppConstant.Speed,this));
        myTTS.setPitch(SharedPref.getFloatInSharedPref(AppConstant.Pitch,this));



        myTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String s) {

            }

            @Override
            public void onDone(String s) {
            if(s.equals("MessageId")){

                 promptSpeechInput();

            }
            }


            @Override
            public void onError(String s) {

            }
        });

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            questionsIdList=new ArrayList<>();
            practiceTestBtnIndex=bundle.getInt(AppConstant.PracTestButtonIndex);
            testType=bundle.getString(AppConstant.TestType);
            questionsIdList=bundle.getStringArrayList(AppConstant.QuestionIdList);
            timerDuration= (int) bundle.getFloat(AppConstant.Duration);

            saveTestRequst.setCategory(bundle.getString(AppConstant.Category));
            saveTestRequst.setTestID(bundle.getString(AppConstant.Id));
            saveTestRequst.setTestName(bundle.getString(AppConstant.TestName));
            saveTestRequst.setUserID(SharedPref.getStringFromSharedPref(AppConstant.Email,this));

            }


            if(testType.equals(AppConstant.Real)){
                testReady=false;
                getTestData();
            }



        nextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(index<questSize){
                    processAnswer(index);
                    removeOptionsTVcolor();
                    index ++;
                    myTTS.stop();
                    setTestType(index);
                    setAllreadyGivenAnswer(index);
                    Animate();
                } }
        });

        startTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTTS.stop();
                previousTV.setVisibility(View.GONE);
                instructionSpoken=false;
                instructionLayout.setVisibility(View.GONE);
                navigRecycler.setVisibility(View.VISIBLE);
                testLayout.setVisibility(View.VISIBLE);

                setTestType(index);
                setTimer(timerDuration);
                }
        });
        repeatTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTTS.stop();
                removeOptionsTVcolor();
                setTestType(index);

            }
        });
        previousTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index>0){
                    processAnswer(index);
                    removeOptionsTVcolor();
                    index --;
                    myTTS.stop();
                    setTestType(index);
                    setAllreadyGivenAnswer(index);
                    Animate();
            }}
        });
        finishTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTTS.stop();
                processAnswer(index);
                if(testType.equals(AppConstant.Real)){
                    savingTextTV.setVisibility(View.VISIBLE);
                    saveTestsResult();
                } else finish();
            }
        });

        setModeToGiveTests();

    }

    private void setModeToGiveTests() {

        if(SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS,this)) {
            setInVisibilityOfButtons();

            option_aTV.setClickable(false);
            option_bTV.setClickable(false);
            option_cTV.setClickable(false);
            option_dTV.setClickable(false);
            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!myTTS.isSpeaking()) {
                        promptSpeechInput();
                    }
                }
            });
        }else {

            setVisibilityOfButtons();
            parentLayout.setClickable(false);

            option_aTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeOptionsTVcolor();
                    changeOptionsColor("a");
                    answer_tv.setText("a");

                }
            });
            option_bTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeOptionsTVcolor();
                    changeOptionsColor("b");
                    answer_tv.setText("b");

                }
            });
            option_cTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeOptionsTVcolor();
                    changeOptionsColor("c");
                    answer_tv.setText("c");
                }
            });
            option_dTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeOptionsTVcolor();
                    changeOptionsColor("d");
                    answer_tv.setText("d");
                }
            });

        }


    }

    private void setInVisibilityOfButtons() {
        nextTV.setVisibility(View.GONE);
        startTV.setVisibility(View.GONE);
        previousTV.setVisibility(View.GONE);
        finishTV.setVisibility(View.GONE);
    }
    private void setVisibilityOfButtons() {
        nextTV.setVisibility(View.VISIBLE);
        startTV.setVisibility(View.VISIBLE);
        //previousTV.setVisibility(View.VISIBLE);
       // finishTV.setVisibility(View.VISIBLE);
    }
    private void removeOptionsTVcolor() {
        option_aTV.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        option_bTV.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        option_cTV.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        option_dTV.setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    private void Animate() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right);
        questCardView.startAnimation(animation);
    }

    private void setTimer(int timerDuration) {
        timerTV.setVisibility(View.VISIBLE);

            int noOfMinutes = timerDuration * 60 * 1000;//Convert minutes into milliseconds
             countDownTimer=   new CountDownTimer(noOfMinutes, 1000) {

                    public void onTick(long millisUntilFinished) {
                        @SuppressLint("DefaultLocale")
                        String hms = String.format("%02d:%02d",
                         TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                         TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                        timerTV.setText("");
                        timerTV.setText(hms);


                    }

                    public void onFinish() {
                        if(testType.equals(AppConstant.Real)){
                        savingTextTV.setVisibility(View.VISIBLE);
                        saveTestsResult();

                    }else finish();
                    }

                }.start();
                }


    private void processAnswer(int index) {
        String answer=answer_tv.getText().toString().trim();
        if(answer.equals("a") ){
            answersList.set(index,option_aTV.getText().toString());
        }
        if(answer.equals("b")){
            answersList.set(index,option_bTV.getText().toString());
        }
        if(answer.equals("c")){
            answersList.set(index,option_cTV.getText().toString());
        }
        if(answer.equals("d")){
            answersList.set(index,option_dTV.getText().toString());
        }
        if(answer_tv.getText().toString().trim().equals("")){
            answersList.set(index,String.valueOf("NotAnswered"));
        }
    }

    private void setTestType(int index) {

        if(testType.equals(AppConstant.Real)){

            setRealTest(index,testLists);

           navigRecycler.setAdapter(new NavigRecyclerAdapter(TestActivity.this,questSize,answersList));

        }
        if(testType.equals(AppConstant.Practice)){

            createPracticeTests();
            setTest(index);
           // navigRecycler.setAdapter(new NavigRecyclerAdapter(TestActivity.this,questSize,answersList));
        }
        if(!SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS,this)){
           if(index==questSize){

              finishTV.setVisibility(View.VISIBLE);
              nextTV.setVisibility(View.INVISIBLE);
            }else {
              finishTV.setVisibility(View.GONE);
              nextTV.setVisibility(View.VISIBLE);
            }

            if(index==0){
              previousTV.setVisibility(View.GONE);
            }else previousTV.setVisibility(View.VISIBLE);

        }
        else {
            if(index==questSize){
              finishTV.setVisibility(View.VISIBLE);
            }else {
              finishTV.setVisibility(View.GONE);
            }

        }
    }

    private void getTestData() {
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        instructionLayout.setVisibility(View.GONE);
        String token="JWT " + SharedPref.getStringFromSharedPref(AppConstant.Token,this);
        Retrofit retrofit=new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConstant.BaseUrl)
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        Call<List<QuestAndOptionResponse>> testDataList=restApis.getQuestAndOPtionResponse(token,questionsIdList);
        testDataList.enqueue(new Callback<List<QuestAndOptionResponse>>() {
            @Override
            public void onResponse(Call<List<QuestAndOptionResponse>> call, Response<List<QuestAndOptionResponse>> response) {
                if(response.body()!=null){
                    List<QuestAndOptionResponse> testDataList=response.body();
                    if(testDataList!=null && testDataList.size()>0){
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        instructionLayout.setVisibility(View.VISIBLE);
                        testLists=testDataList;
                        testReady=true;
                        onInit(0);
                        questSize=testDataList.size()-1;

                        for(int i=0;i<testDataList.size();i++){
                            answersList.add("NoData");
                        }

                        navigRecycler.setAdapter(new NavigRecyclerAdapter(TestActivity.this,questSize,answersList));



                       }
                }
            }

            @Override
            public void onFailure(Call<List<QuestAndOptionResponse>> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);

                Snackbar snackbar=Snackbar.make(coordinatorLayout,"something went wrong", Snackbar.LENGTH_LONG);
                snackbar.show();
                Intent intent=new Intent(TestActivity.this,HomeActivity.class);
                startActivity(intent);

            }
        });

    }




    private void initViews() {
        nextTV=findViewById(R.id.next_tv);
        startTV=findViewById(R.id.start_tv);
        finishTV=findViewById(R.id.finish_tv);
        previousTV=findViewById(R.id.previous_tv);
        answer_tv=findViewById(R.id.answer_tv);
        questionsTV=findViewById(R.id.quest_tv);
        instructTV1=findViewById(R.id.inst_tv1);
        instructTV2=findViewById(R.id.inst_tv2);
        instructTV3=findViewById(R.id.inst_tv3);
        instructTV4=findViewById(R.id.inst_tv4);
        instructTV5=findViewById(R.id.inst_tv5);
        instructTV6=findViewById(R.id.inst_tv6);
        timerTV=findViewById(R.id.timer_tv);
        option_aTV=findViewById(R.id.option_a_tv);
        option_bTV=findViewById(R.id.option_b_tv);
        option_cTV=findViewById(R.id.option_c_tv);
        option_dTV=findViewById(R.id.option_d_tv);
        questNoTV=findViewById(R.id.quest_no_tv);
        navigRecycler=findViewById(R.id.navig_recycler);
        drawerLayout=findViewById(R.id.drrawer_layout);
        toolbar=findViewById(R.id.tool_bar);
        savingTextTV=findViewById(R.id.saving_text);
        coordinatorLayout=findViewById(R.id.coordinator_layout);
        testLayout=findViewById(R.id.linear_testlayout);
        instructionLayout=findViewById(R.id.linear_instruction_layout);
        repeatTV=findViewById(R.id.repeat_tv);
        questCardView=findViewById(R.id.cardview);
        dropDownSpinner=findViewById(R.id.dropdown_spinner);
        parentLayout=findViewById(R.id.parent_frame_layout);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

    }


    private void createPracticeTests() {
        questions = new ArrayList<>();
        options_a = new ArrayList<>();
        options_b = new ArrayList<>();
        options_c = new ArrayList<>();
        options_d = new ArrayList<>();
        correctAnswer = new ArrayList<>();


        questions.add("Who is the Prime Minister of India");
        options_a.add("Rahul Gandhi");
        options_b.add("Arvind kejriwal");
        options_c.add("Narendra modi");
        options_d.add("Lalu prasad yadav");
        correctAnswer.add("Narendra Modi");

        questions.add("When is the next Cricket 50-50 WorldCup");
        options_a.add("2019");
        options_b.add("2020");
        options_c.add("2021");
        options_d.add("2022");
        correctAnswer.add("Jaane Gaane Maane");

        questions.add("Which one of these National Anthem of India");
        options_a.add("Vande Matram");
        options_b.add("Jai Ho");
        options_c.add("Jaane Gaane Maane");
        options_d.add("None Of the Above");
        correctAnswer.add("2019");

        questSize=questions.size()-1;
        for(int i=0;i<questions.size();i++){
            answersList.add("NoData");
        }
        navigRecycler.setAdapter(new NavigRecyclerAdapter(this,questSize,answersList));

    }

    private void setTest(int index) {

            questionsTV.setText(questions.get(index));
            questNoTV.setText("Q"+(index+1)+": ");
            option_aTV.setText(options_a.get(index));
            option_bTV.setText(options_b.get(index));
            option_cTV.setText(options_c.get(index));
            option_dTV.setText(options_d.get(index));
            answer_tv.setText("");

        speakWord(questionsTV.getText().toString());
        speakWord("option a");
        speakWord( option_aTV.getText().toString());
        speakWord("option b");
        speakWord( option_bTV.getText().toString());
        speakWord("option c");
        speakWord( option_cTV.getText().toString());
        speakWord("option d");
        speakWord(option_dTV.getText().toString());

        speakWordToShowSpeechInputDialog(" say repeat to repeat the question or skip to skip the question");



    }

    private void speakWord(String speech) {
      if (SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS, this)) {
          if (myTTS != null) {
              myTTS.speak(speech, TextToSpeech.QUEUE_ADD, null);
              }

        } else {
          myTTS.stop();


        }
    }
    private void speakWordToShowSpeechInputDialog(String speech){
        if (SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS, this)) {
            if(myTTS!=null) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
                myTTS.speak(speech, TextToSpeech.QUEUE_ADD,map);
            }
        }else {
            myTTS.stop();

    }}

    private void setRealTest(int index, List<QuestAndOptionResponse> testDataList) {
        questionsTV.setText(testDataList.get(index).getText());
        questNoTV.setText("Q"+(index+1)+": ");
        option_aTV.setText(testDataList.get(index).getOptions().get(0));
        option_bTV.setText(testDataList.get(index).getOptions().get(1));
        option_cTV.setText(testDataList.get(index).getOptions().get(2));
        option_dTV.setText(testDataList.get(index).getOptions().get(3));
        answer_tv.setText("");

        speakWord(questionsTV.getText().toString());
        speakWord("option a");
        speakWord( option_aTV.getText().toString());
        speakWord("option b");
        speakWord( option_bTV.getText().toString());
        speakWord("option c");
        speakWord( option_cTV.getText().toString());
        speakWord("option d");
        speakWord(option_dTV.getText().toString());

        speakWordToShowSpeechInputDialog("say repeat to repeat the question or skip to skip the question");

        }

    @Override
    public void onInit(int i) {

        if(testReady) {
                if (SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS, this)) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
                    myTTS.speak("Do you Want to hear the instructions say yes or no", TextToSpeech.QUEUE_FLUSH,map);
                }else {
                    myTTS.stop();
                }


        }
        }

    private void speakInstruction() {

        myTTS.speak(instructTV1.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        myTTS.speak(instructTV2.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        myTTS.speak(instructTV3.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        myTTS.speak(instructTV4.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        myTTS.speak(instructTV5.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        myTTS.speak(instructTV6.getText().toString(), TextToSpeech.QUEUE_ADD, null);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        myTTS.speak("say start to start the test", TextToSpeech.QUEUE_ADD,map);



    }
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    processSpeechData(result);

                    }
                break;
            }

        }
    }

    private void processSpeechData(ArrayList<String> result) {
        boolean matches=true;

        for(int index=0 ;index<result.size() ;index++){
                 if(matches){

            if(result.get(index).equals("a") || result.get(index).equals("b") || result.get(index).equals("c") ||
                    result.get(index).equals("d")){
                    if(! instructionSpoken) {
                      answer_tv.setText(result.get(index));
                      changeOptionsColor(result.get(index));
                      optionSpoken=true;

                      speakWordToShowSpeechInputDialog("Do you want to change the answer say yes or no");
                      }else
                          {
                      speakWordToShowSpeechInputDialog("Your answer do not match  say only yes and no");
                      }
                      matches = false;
                }
               else if(result.get(index).equals("skip") || result.get(index).equals("skeep")){
                     if(! instructionSpoken){

                       nextTV.performClick();
                     }else {
                      speakWordToShowSpeechInputDialog("Your answer do not match  please speek again");
                     }
                     matches=false;
                }
                else if(result.get(index).equals("yes")){
                      if(instructionSpoken){
                       instructionListened=true;
                       speakInstruction();



                     }else{
                         if(optionSpoken){
                              removeOptionsTVcolor();
                              speakWordToShowSpeechInputDialog("Speak your new answer");
                          }else {
                             optionSpoken=false;
                             speakWordToShowSpeechInputDialog("Your answer do not match please speak again or to skip the question say skip");

                         }


                     }

                      matches=false;
                }
                else if(result.get(index).equals("no")){
                      if(instructionSpoken){
                          if(!instructionListened){
                              startTV.performClick();
                              instructionSpoken=false;
                          }else {
                              speakWordToShowSpeechInputDialog("Your answer do not match please speak again");

                          }

                      }
                      else
                          {
                      nextTV.performClick();
                      }

                      matches=false;
                }else if(result.get(index).equals("repeat")){
                     if(!instructionSpoken){
                       repeatTV.performClick();
                       instructionSpoken=false;
                      }else
                      {
                      speakWordToShowSpeechInputDialog("Your answer do not match please speak again");

                     }
                matches=false;
                }else if(result.get(index).equals("start")){

                    if(instructionSpoken) {
                      startTV.performClick();
                }   else {
                      speakWordToShowSpeechInputDialog("Your answer do not match please speak again");

                }
            }
           else {
                  speakWordToShowSpeechInputDialog("Your answer do not match please speak again");
                  matches=false;
            }

        }
                }

    }

    private void changeOptionsColor(String s) {
        if(s.equals("a")){
           option_aTV.setBackgroundColor(getResources().getColor(R.color.fragment_color));
        }
        if(s.equals("b")){
            option_bTV.setBackgroundColor(getResources().getColor(R.color.fragment_color));
        }
        if(s.equals("c")){
            option_cTV.setBackgroundColor(getResources().getColor(R.color.fragment_color));
        }
        if(s.equals("d")){
            option_dTV.setBackgroundColor(getResources().getColor(R.color.fragment_color));
        }


    }

    private void saveTestsResult() {
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        testLayout.setVisibility(View.GONE);

        saveTestRequst.setAnswer(answersList);
        String token="JWT "+SharedPref.getStringFromSharedPref(AppConstant.Token,this);

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        Call<SaveTestResponse> saveTestResponseCall=restApis.getTestResponse(token,saveTestRequst);
                saveTestResponseCall.enqueue(new Callback<SaveTestResponse>() {
                    @Override
                    public void onResponse(Call<SaveTestResponse> call, Response<SaveTestResponse> response) {
                        if(response.body()!=null){

                            final SaveTestResponse saveTestResponse=response.body();
                            assert saveTestResponse != null;
                            SharedPref.saveStringInSharedPref(AppConstant._ID,saveTestResponse.get_id(),TestActivity.this);
                            Snackbar snackbar=Snackbar.make(coordinatorLayout,"Test Saved, See Result on Completed Test Screen", Snackbar.LENGTH_LONG);
                             snackbar.show();
                             snackbar.setDuration(2000);
                             if(snackbar.isShown()){

                                 setUinotClickable();

                                 Handler handler=new Handler();
                                 handler.postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         Intent intent=new Intent(TestActivity.this,ResultActivity.class);
                                         intent.putExtra(AppConstant.TestID,saveTestResponse.getTestID());
                                         intent.putExtra(AppConstant.UserAnswersList,answersList);
                                         intent.putExtra(AppConstant.TestName,saveTestResponse.getTestName());
                                         intent.putExtra("launchFromTestPage",true);
                                         startActivity(intent);
                                         //finish();
                                     }
                                 },1000);
                             }

                        }else {
                            Snackbar snackbar=Snackbar.make(coordinatorLayout,"something went wrong please try again", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        testLayout.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onFailure(Call<SaveTestResponse> call, Throwable t) {
                        testLayout.setVisibility(View.VISIBLE);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        Snackbar snackbar=Snackbar.make(coordinatorLayout,"something went wrong please try again", Snackbar.LENGTH_LONG);
                        snackbar.show();

                    }
                });

    }

    private void setUinotClickable() {
        nextTV.setClickable(false);
        finishTV.setClickable(false);
        parentLayout.setClickable(false);
        option_aTV.setClickable(false);
        option_bTV.setClickable(false);
        option_cTV.setClickable(false);
        option_dTV.setClickable(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myTTS.stop();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder ab=new AlertDialog.Builder(TestActivity.this);
        ab.setCancelable(false);
        ab.setTitle("Are you sure want to exit?");
        ab.setPositiveButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        ab.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick( DialogInterface dialog, int which) {
                myTTS.shutdown();

                if (!instructionSpoken && testType.equals(AppConstant.Real)) {

                    showSaveTestConfirmationDialog();
                    dialog.dismiss();
                    }
                    else{
                    dialog.dismiss();
                    finish();
                }


            }
        });
        ab.show();

    }

    private void showSaveTestConfirmationDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(TestActivity.this);
        adb.setCancelable(false);
        adb.setTitle("Do You Want to save your Test");
        adb.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                countDownTimer.cancel();
                savingTextTV.setVisibility(View.VISIBLE);
                saveTestsResult();

            }
        });
        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                countDownTimer.cancel();
                dialogInterface.dismiss();
                finish();

            }
        });adb.show();

    }


    @Override
    public void questIndexClicked(int questNo) {
        myTTS.stop();
        drawerLayout.closeDrawer(Gravity.RIGHT);
        processAnswer(index);
        removeOptionsTVcolor();
        index=questNo;
        setTestType(index);
        setAllreadyGivenAnswer(questNo);


//        if(index==answersList.size()-1){
//            finishTV.setVisibility(View.VISIBLE);
//        }else {
//            finishTV.setVisibility(View.GONE);
//            nextTV.setVisibility(View.VISIBLE);
//        }
//
//        if(testType.equals(AppConstant.Real)){
//
//            setRealTest(index,testLists);
//        }
//        else if(testType.equals(AppConstant.Practice)){
//
//           // createPracticeTests();
//            setTest(index);
//        }

    }

    private void setAllreadyGivenAnswer(int questNo) {
        String answer=answersList.get(questNo).toString().trim();
        if(option_aTV.getText().toString().trim().equals(answer)){
            option_aTV.performClick();

        }else if(option_bTV.getText().toString().trim().equals(answer)){
            option_bTV.performClick();

        }else if(option_cTV.getText().toString().trim().equals(answer)){
            option_cTV.performClick();

        }else if(option_dTV.getText().toString().trim().equals(answer)){
            option_dTV.performClick();

        }else {
            removeOptionsTVcolor();
        }
    }

    private void setDropDownTestType() {
        ArrayList<String> testType=new ArrayList<>();
        testType.add("Text to Speech");
        testType.add("Normal test");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TestActivity.this, android.R.layout.simple_spinner_dropdown_item, testType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropDownSpinner.setAdapter(adapter);

        if(SharedPref.getBooleanInSharedPref(AppConstant.TestTypeTTS,TestActivity.this)){
            dropDownSpinner.setSelection(0);
        }else {
            dropDownSpinner.setSelection(1);
        }




        dropDownSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
               // int index = adapterView.getSelectedItemPosition();
                ((TextView) dropDownSpinner.getSelectedView()).setTextColor(getResources().getColor(android.R.color.white));


                switch (position) {

                    case 0:
                        SharedPref.saveBooleanInSharedPref(AppConstant.TestTypeTTS,true,TestActivity.this);
                        Snackbar snackbar= Snackbar.make(parentLayout,"Test Type SPEECH MODE Selected",Snackbar.LENGTH_LONG);
                        snackbar.show();
                        if(!instructionSpoken){
                            repeatTV.performClick();

                        }else {
                          onInit(0);
                        }
                        setModeToGiveTests();
                        break;


                    case 1:
                        SharedPref.saveBooleanInSharedPref(AppConstant.TestTypeTTS,false,TestActivity.this);
                        Snackbar snackbars=  Snackbar.make(parentLayout,"Test Type NORMAL MODE Selected",Snackbar.LENGTH_LONG);
                        snackbars.show();
                        setModeToGiveTests();
                        myTTS.stop();
                        break;
                }}

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

}




