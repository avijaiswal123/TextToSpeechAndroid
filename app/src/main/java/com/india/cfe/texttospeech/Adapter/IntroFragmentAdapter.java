package com.india.cfe.texttospeech.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.india.cfe.texttospeech.Fragments.IntroScreenFragments.IntroFragment_1;
import com.india.cfe.texttospeech.Fragments.IntroScreenFragments.IntroFragment_2;
import com.india.cfe.texttospeech.Fragments.IntroScreenFragments.IntroFragment_3;
import com.india.cfe.texttospeech.Fragments.IntroScreenFragments.IntroFragment_4;

/**
 * Created by Admin on 7/8/2018.
 */

public class IntroFragmentAdapter extends FragmentPagerAdapter {
    public IntroFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new IntroFragment_1();
            case 1:
                return new IntroFragment_2();
            case 2:
                return new IntroFragment_3();
            case 3:
                return new IntroFragment_4();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        View view = (View) object;
//        container.removeView(view);
    }
}
