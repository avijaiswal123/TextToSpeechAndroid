package com.india.cfe.texttospeech.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.india.cfe.texttospeech.Adapter.IntroFragmentAdapter;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.SharedPref;


public class IntroductionActivity extends AppCompatActivity implements android.speech.tts.TextToSpeech.OnInitListener{
    private ViewPager viewPager;
    private IntroFragmentAdapter fragmentAdapter;
    private Button btnSkip, btnNext;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private TextToSpeech textToSpeech;
    private View bottomBar;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_introduction);
        textToSpeech=new TextToSpeech(this,this);
        viewPager=findViewById(R.id.view_pager);
        btnNext=findViewById(R.id.btn_next);
        btnSkip=findViewById(R.id.btn_skip);
        dotsLayout = findViewById(R.id.layoutDots);
        bottomBar=findViewById(R.id.bottom_bar);
        textToSpeech=new TextToSpeech(this,this);


        viewPager.addOnPageChangeListener(pageChangeListener);
        addBottomsDots(0);
        changeStatusBarColor();
        fragmentAdapter=new IntroFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);

        btnSkip.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setOneVisibility();
            Intent intent=new Intent(IntroductionActivity.this,LoginActivity.class);
            startActivity(intent);


        }
         });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current=getItem(+1);
                if (current < 4) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                  //  launchHomeScreen();
                    setOneVisibility();
                    Intent intent=new Intent(IntroductionActivity.this,LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

//        viewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return true;
//            }
//        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        textToSpeech.stop();
    }

    private void setOneVisibility() {
        SharedPref.saveBooleanInSharedPref(AppConstant.IsWelcomed,true,this);

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void addBottomsDots(int currentPage) {
        dots=new TextView[4];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
       
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);

    }
    ViewPager.OnPageChangeListener pageChangeListener=new ViewPager.OnPageChangeListener() {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        addBottomsDots(position);
        if(position==0){
            textToSpeech.speak("Welcome To Hands Free",TextToSpeech.QUEUE_FLUSH,null);
        }
        if(position==1){
            bottomBar.setBackgroundColor(Color.BLACK);
           btnNext.setTextColor(Color.BLACK);
           btnSkip.setTextColor(Color.BLACK);
            textToSpeech.speak("Handsfree Lets The Users To Practice As Well As Give Test With Users Voice",TextToSpeech.QUEUE_FLUSH,null);

        }
        if(position==2){
            btnNext.setTextColor(Color.WHITE);
            btnSkip.setTextColor(Color.WHITE);
            textToSpeech.speak("Speak Your Heart Out",TextToSpeech.QUEUE_FLUSH,null);
        }

        if (position == 3) {
            // last page. make button text to GOT IT
            btnNext.setText(getString(R.string.start));
            bottomBar.setBackgroundColor(Color.WHITE);
            btnNext.setTextColor(Color.WHITE);
            btnSkip.setVisibility(View.GONE);
            textToSpeech.speak("Results Are Shown IN Form Of Graphs",TextToSpeech.QUEUE_FLUSH,null);
        } else {
            // still pages are left
            btnNext.setText(getString(R.string.next));
            btnSkip.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {


    }
};

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onInit(int i) {
        textToSpeech.speak("Welcome To Hands Free",TextToSpeech.QUEUE_FLUSH,null);
    }
}
