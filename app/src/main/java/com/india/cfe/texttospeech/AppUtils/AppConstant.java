package com.india.cfe.texttospeech.AppUtils;

public class AppConstant {
    public static final String BaseUrl="https://candidate-cfe.herokuapp.com";
    public static final String Token="token";
    public static final String Id="id";
    public static final String Email="email";
    public static final String SourceApp="sourceApp";
    public static final String Status="status";
    public static final String UserName="userName";
    public static final String QuestionIdList="questionIdList";
    public static final String IsLogined="logined";
    public static final String IsWelcomed="welcomed";
    public static final String PracTestButtonIndex="pracTestBtnIndex";
    public static  String TestType;
    public static final String Real="real";
    public static final String Practice="practice";
    public static final String TestName="testName";
    public static final String Category="category";
    public static final String Volume="Volume Level";
    public static final String Speed="Speed Rate";
    public static final String Pitch="Pitch Rate";
    public static final String _ID="_id";
    public static final String Duration="duration";
    public static final String EncodedImage="image";
    public static final String TestID="testid";
    public static final String Registered="registered";
    public static final String UserAnswersList="answerslist";
    public static final String NoData="NoData";
    public static final String NotAswered="NotAnswered";
    public static final String TestTypeTTS="testTypeTTS";
    public static final String RightAnswer="rightanswer";
    public static final String WrongAnswer="wronganswer";
    public static final String NoAnswer="noanswer";








}
//"/tests/getassignedtests"
//headers=