package com.india.cfe.texttospeech.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.india.cfe.texttospeech.ModelClass.QuestAndOptionResponse;
import com.india.cfe.texttospeech.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class NavigRecyclerAdapter extends RecyclerView.Adapter<NavigRecyclerAdapter.ViewHolder> {
    private Context mContext;
    private int questSize;
    private ArrayList<String> answerList;
    public NavigRecyclerAdapter(Context context, int questSize,ArrayList<String> answerList) {
        this.mContext=context;
        this.answerList=answerList;
        this.questSize=questSize;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.navig_recycler_adapter,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.navigBtn.setText(""+(position + 1));
        if(answerList!=null){

                if(answerList.get(position).equals("NotAnswered")){
                   holder.navigBtn.setBackgroundResource(R.color.nav_btn_red);
                }
                else if (answerList.get(position).equals("NoData"))
                    holder.navigBtn.setBackgroundResource(R.color.nav_btn_white);
                else  holder.navigBtn.setBackgroundResource(R.color.nav_btn_green);

        }
        holder.navigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnQuestClick questClick= (OnQuestClick) mContext;
                questClick.questIndexClicked(position);


            }
        });

    }

    @Override
    public int getItemCount() {
        return questSize+1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button navigBtn;
        public ViewHolder(View itemView) {
            super(itemView);

            navigBtn = itemView.findViewById(R.id.navig_btn);
        }
    }public interface OnQuestClick{

        void questIndexClicked(int questNo);
    }
}
