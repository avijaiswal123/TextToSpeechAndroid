package com.india.cfe.texttospeech.Fragments.HomeScreenFragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.india.cfe.texttospeech.Adapter.CompletedTestAdapter;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.RestApis;
import com.india.cfe.texttospeech.AppUtils.SharedPref;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResponse;
import com.india.cfe.texttospeech.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompletedFragment extends Fragment{
    private RecyclerView comp_recycler;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;
    private CoordinatorLayout coordinatorLayout;
    private LinearLayout noInternetLayout,noTestLayout;
    private FrameLayout parentLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    private PieChart pieChart;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.completed_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        comp_recycler=view.findViewById(R.id.comp_recycler);
        layoutManager=new LinearLayoutManager(getContext());
        comp_recycler.setLayoutManager(layoutManager);
        parentLayout=view.findViewById(R.id.linear_layout);
        coordinatorLayout=view.findViewById(R.id.coordinator_layout);
        refreshLayout=view.findViewById(R.id.swipe_layout);
        noTestLayout=view.findViewById(R.id.no_test_linear);
        pieChart=view.findViewById(R.id.piechart);
        noInternetLayout=view.findViewById(R.id.no_internet_linear);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(checkNetworkConnection()){
                    getCompletedTestData();

                }else {
                    noTestLayout.setVisibility(View.GONE);
                    noInternetLayout.setVisibility(View.VISIBLE);
                    comp_recycler.setVisibility(View.GONE);

                }
                refreshLayout.setRefreshing(false);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if(checkNetworkConnection()) {
            getCompletedTestData();
        }else {
            noTestLayout.setVisibility(View.GONE);
            noInternetLayout.setVisibility(View.VISIBLE);
            comp_recycler.setVisibility(View.GONE);
        }
    }

    private void getCompletedTestData() {
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        noTestLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
        comp_recycler.setVisibility(View.GONE);
        String token="JWT "+ SharedPref.getStringFromSharedPref(AppConstant.Token,getContext());
        String userId= SharedPref.getStringFromSharedPref(AppConstant.Email,getContext());
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        Call<List<CompletedTestResponse>> completedTestResponse=restApis.getCompletedTestResponse(token,userId);
        completedTestResponse.enqueue(new Callback<List<CompletedTestResponse>>() {
            @Override
            public void onResponse(Call<List<CompletedTestResponse>> call, Response<List<CompletedTestResponse>> response) {

                if(response.body()!=null && response.body().size()>0){
                    List<CompletedTestResponse> testResponse=response.body();
                    if(testResponse.get(0).getTestName()!=null) {
                        Collections.reverse(testResponse);

                        setDataOnPieChart();

                        CompletedTestAdapter completeTestAdapter = new CompletedTestAdapter(getContext(), testResponse);
                        comp_recycler.setAdapter(completeTestAdapter);
                        comp_recycler.setVisibility(View.VISIBLE);
                    }else {
                        noTestLayout.setVisibility(View.VISIBLE);
                    }
                }else {
                    noTestLayout.setVisibility(View.VISIBLE);
                }

                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<CompletedTestResponse>> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                noTestLayout.setVisibility(View.VISIBLE);

                Snackbar snackbar=Snackbar.make(coordinatorLayout,"Something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });


    }

    public boolean checkNetworkConnection() {
        boolean networkStatus = false;
        ConnectivityManager conMgr = (ConnectivityManager) (getContext()).getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        networkStatus = netInfo != null;
        return networkStatus;
    }
   public void setDataOnPieChart(){


       ArrayList<Integer> resultList=new ArrayList<>();
       resultList.add(SharedPref.getIntegerFromSharedPref(AppConstant.WrongAnswer,getContext()));
       resultList.add(SharedPref.getIntegerFromSharedPref(AppConstant.RightAnswer,getContext()));
       resultList.add(SharedPref.getIntegerFromSharedPref(AppConstant.NoAnswer,getContext()));


           String answersType[]={"Wrong","Correct","NotAnswered"};
           // Integer results[]={20,50,30};
           List<PieEntry> pieEntries=new ArrayList<>();
           for(int i=0 ; i<answersType.length ;i++){
               pieEntries.add(new PieEntry(resultList.get(i),answersType[i]));
           }
           PieDataSet pieDataSet=new PieDataSet(pieEntries,"");
           pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
           PieData pieData=new PieData(pieDataSet);
           pieChart.setData(pieData);
           pieChart.animateY(2000);
           pieChart.invalidate();




//        }

   }



}
