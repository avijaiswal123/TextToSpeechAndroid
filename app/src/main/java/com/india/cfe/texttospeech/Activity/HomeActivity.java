package com.india.cfe.texttospeech.Activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;



import com.india.cfe.texttospeech.Adapter.HomeScreenFragmentAdapter;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.SharedPref;
import com.india.cfe.texttospeech.AppUtils.ViewPagerAnimation;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.CompletedFragment;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.PracticeFragment;
import com.india.cfe.texttospeech.Fragments.HomeScreenFragments.TestFragment;
import com.india.cfe.texttospeech.R;

import java.util.Objects;

public class HomeActivity extends AppCompatActivity {
    private  ViewPager viewPager;
    private  TabLayout tabLayout;
    private  Toolbar toolbar;
    private  HomeScreenFragmentAdapter fragmentAdapter;
    private int[] tabIcons = {
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        viewPager=findViewById(R.id.viewpager);
        tabLayout=findViewById(R.id.tablayout);
        toolbar=findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
      // (getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        fragmentAdapter=new HomeScreenFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setPageTransformer(true,new ViewPagerAnimation());
       // setupTabIcons();


//        viewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return true;
//            }
//        });

    }


    private void setupTabIcons() {
        (tabLayout.getTabAt(0)).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder ab=new AlertDialog.Builder(HomeActivity.this);
        ab.setCancelable(false);
        ab.setTitle("Are you sure want to exit?");

        ab.setPositiveButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        ab.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        ab.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.logout:
                logout();
                break;
            case R.id.settingActivity:
            Intent intent=new Intent(this,SettingActivity.class);
            startActivity(intent);
            break;


        }
        return true;
    }

    private void logout() {
        AlertDialog.Builder ab=new AlertDialog.Builder(HomeActivity.this);
        ab.setCancelable(false);
        ab.setTitle("Are you sure want to Logout?");

        ab.setPositiveButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        ab.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPref.saveBooleanInSharedPref(AppConstant.IsLogined,false,HomeActivity.this);
                Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
        ab.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
