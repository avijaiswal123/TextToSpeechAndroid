package com.india.cfe.texttospeech.ModelClass;

import java.io.Serializable;

public class RegistrationResponse implements Serializable{
    private String msg;
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
