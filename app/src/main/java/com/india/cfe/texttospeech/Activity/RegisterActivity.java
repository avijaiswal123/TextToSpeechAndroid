package com.india.cfe.texttospeech.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.AppUtils.SharedPref;
import com.india.cfe.texttospeech.ModelClass.RegistrationRequest;
import com.india.cfe.texttospeech.ModelClass.RegistrationResponse;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.RestApis;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {
    private EditText nameET, emailET, pswdET, confirmPswdET;
    private Button registerBtn;
    private TextView loginTV;
    private LinearLayout registerLayout;
    private CoordinatorLayout coordinatorLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nameET = findViewById(R.id.name);
        emailET = findViewById(R.id.email);
        pswdET = findViewById(R.id.password);
        loginTV=findViewById(R.id.login_tv);
        confirmPswdET = findViewById(R.id.confirm_password);
        registerBtn = findViewById(R.id.regiter_btn);
        registerLayout=findViewById(R.id.linear_register_layout);
        coordinatorLayout=findViewById(R.id.coordinatorLayout);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetworkConnection()){

                if (nameET.getText().toString().trim().length() == 0) {
                    nameET.setError("Enter name first");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString().trim()).matches()) {
                    emailET.setError("Enter valid email");
                    emailET.setText("");
                } else if (pswdET.getText().toString().trim().length() < 8) {
                    pswdET.setError("Password must be of atleast 8 characters with combination of alpha numeric and special characters");
                    pswdET.setText("");

                } else if (!confirmPswdET.getText().toString().equals(pswdET.getText().toString())) {
                    confirmPswdET.setError("Password do not match");
                } else {
                    RegistrationRequest registrationRequest = new RegistrationRequest();
                    registrationRequest.setInsName(nameET.getText().toString());
                    registrationRequest.setEmail(emailET.getText().toString());
                    registrationRequest.setPassword(pswdET.getText().toString());

                    doRegistration(registrationRequest);
                }
                }
                else {
                    Snackbar snackbar=Snackbar.make(coordinatorLayout,"No Internet",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

    }


    private void doRegistration(final RegistrationRequest registrationRequest) {
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        registerLayout.setVisibility(View.GONE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis = retrofit.create(RestApis.class);
        final Call<RegistrationResponse> response = restApis.getRegisterResponse(registrationRequest);
        response.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.body() != null) {
                    RegistrationResponse result = response.body();
                    SharedPref.saveStringInSharedPref(AppConstant.UserName,registrationRequest.getInsName(),RegisterActivity.this);

                    Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                    intent.putExtra(AppConstant.Registered,"Registration Sucessfull");
                    startActivity(intent);


                } else{



                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Snackbar  snackbar = Snackbar.make(coordinatorLayout,jObjError.getString("msg"),Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                registerLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                registerLayout.setVisibility(View.VISIBLE);

                Snackbar snackbar=Snackbar.make(coordinatorLayout,"Something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();


            }
        });

    }

    public boolean checkNetworkConnection() {
        boolean networkStatus = false;
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        networkStatus = netInfo != null;
        return networkStatus;
    }
}