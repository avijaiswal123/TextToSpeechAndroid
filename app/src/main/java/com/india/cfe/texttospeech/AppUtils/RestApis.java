package com.india.cfe.texttospeech.AppUtils;

import com.india.cfe.texttospeech.Adapter.TestCategoryList;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResponse;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResult;
import com.india.cfe.texttospeech.ModelClass.QuestAndOptionResponse;
import com.india.cfe.texttospeech.ModelClass.LoginRequest;
import com.india.cfe.texttospeech.ModelClass.LoginResponse;
import com.india.cfe.texttospeech.ModelClass.RegistrationRequest;
import com.india.cfe.texttospeech.ModelClass.RegistrationResponse;
import com.india.cfe.texttospeech.ModelClass.SaveTestRequst;
import com.india.cfe.texttospeech.ModelClass.SaveTestResponse;
import com.india.cfe.texttospeech.ModelClass.UserTestsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApis {

    @POST("/users/register")
    Call<RegistrationResponse> getRegisterResponse(@Body RegistrationRequest registrationRequest);

    @POST("/users/authenticate")
    Call<LoginResponse> getLoginResponse(@Body LoginRequest loginRequest);

  //  @Headers( "Content-Type: application/json" )
    @GET ("/tests/getassignedtests")
    Call<List<UserTestsResponse>> getUserTestResponse(@Query("category") String query);

    @Headers( "Content-Type: application/json" )
    @PUT("/tests/getQuestions")
    Call<List<QuestAndOptionResponse>> getQuestAndOPtionResponse(@Header("Authorization") String auth, @Body ArrayList<String> questId);

    @Headers( "Content-Type: application/json" )
    @PUT("/tests/saveans")
    Call<SaveTestResponse> getTestResponse(@Header("Authorization") String auth, @Body SaveTestRequst saveTest);

    @Headers( "Content-Type: application/json" )
    @GET("tests/appresult/{id}")
    Call<List<CompletedTestResponse>> getCompletedTestResponse(@Header("Authorization") String auth, @Path("id") String id);

    @PUT("/tests/getQuestions")
    Call<List<CompletedTestResult>> getTestResults(@Header("Authorization") String auth ,@Body ArrayList<String> testId);

    @GET("/tests/getDistinct")
    Call<TestCategoryList> getTestCategories(@Header("Authorization") String auth);




}


