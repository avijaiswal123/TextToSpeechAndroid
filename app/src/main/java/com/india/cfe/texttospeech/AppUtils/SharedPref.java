package com.india.cfe.texttospeech.AppUtils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private static final String DBNAME = "mydatabase";
    private static SharedPreferences mPreferences;
    //private SharedPreferences.Editor editor;

    private static SharedPreferences getPreferences(Context context){
        if(mPreferences==null)
            mPreferences=context.getSharedPreferences(DBNAME,Context.MODE_PRIVATE);
        return mPreferences;
    }
    public static void saveStringInSharedPref(String key,String value,Context context){
       SharedPreferences.Editor editor=getPreferences(context).edit();
        editor.putString(key,value);
        editor.apply();
    }
    public static void saveBooleanInSharedPref(String key,boolean value,Context context){
        SharedPreferences.Editor editor=getPreferences(context).edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
    public static String getStringFromSharedPref(String key,Context context){
        return getPreferences(context).getString(key,"0");
    }
    public static boolean getBooleanInSharedPref(String key,Context context){
        return getPreferences(context).getBoolean(key, false);
    }
    public static void saveFloatInSharedPref(String key,Float value,Context context){
        SharedPreferences.Editor editor=getPreferences(context).edit();
        editor.putFloat(key,value);
        editor.apply();
    } public static Float getFloatInSharedPref(String key,Context context){
        return getPreferences(context).getFloat(key,0);
    }

    public static void saveIntegerInSharedPref(String key,int value,Context context){
        SharedPreferences.Editor editor=getPreferences(context).edit();
        editor.putInt(key,value);
        editor.apply();
    } public static int getIntegerFromSharedPref(String key,Context context){
        return getPreferences(context).getInt(key,0);
    }

}
