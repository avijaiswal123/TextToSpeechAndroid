package com.india.cfe.texttospeech.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.facebook.shimmer.ShimmerFrameLayout;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.ModelClass.LoginRequest;
import com.india.cfe.texttospeech.ModelClass.LoginResponse;
import com.india.cfe.texttospeech.R;
import com.india.cfe.texttospeech.AppUtils.RestApis;
import com.india.cfe.texttospeech.AppUtils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    private EditText email_et,password_et;
    private Button loginBtn;
    private TextView signUP_tv;
    private CoordinatorLayout coordinatorLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    private LinearLayout loginLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email_et=findViewById(R.id.email);
        password_et=findViewById(R.id.password);
        signUP_tv=findViewById(R.id.signup_tv);
        loginLayout=findViewById(R.id.linear_login_layout);
        coordinatorLayout=findViewById(R.id.coordinatorLayout);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            Snackbar snackbar=Snackbar.make(coordinatorLayout,bundle.getString(AppConstant.Registered),Snackbar.LENGTH_LONG);
            snackbar.show();
        }

        //progressBar=new ProgressBar(LoginActivity.this,null,R.attr.progressBarStyle);
       // RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(100,100);
       // params.addRule(RelativeLayout.CENTER_IN_PARENT);

        loginBtn=findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkNetworkConnection()){
                if(email_et.getText().toString().trim().length()<=4){
                    email_et.setError("Enter valid Email");
                }
                else if(password_et.getText().toString().length()<=7) {
                    password_et.setError("Wrong password");
                    password_et.setText("");
                }
                else {

                    LoginRequest loginRequest=new LoginRequest();
                    loginRequest.setEmail(email_et.getText().toString());
                    loginRequest.setPassword(password_et.getText().toString());
                    doLogin(loginRequest);
                }

            }else { Snackbar snackbar=Snackbar.make(coordinatorLayout,"No Internet",Snackbar.LENGTH_LONG);
                    snackbar.show();}
            }
        });
        signUP_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doLogin(LoginRequest loginRequest) {
        loginLayout.setVisibility(View.GONE);
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(AppConstant.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApis restApis=retrofit.create(RestApis.class);
        Call<LoginResponse> loginResponse=restApis.getLoginResponse(loginRequest);
        loginResponse.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.body()!=null){
                    LoginResponse loginResponse1=response.body();
                    saveTheResponseInSharedPrefeces(loginResponse1);


                    Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(intent);

                    }else {

                    JSONObject jObjError = null;
                    try {
                        assert response.errorBody() != null;
                        jObjError = new JSONObject(response.errorBody().string());
                        Snackbar snackbar=Snackbar.make(coordinatorLayout,jObjError.getString("msg"),Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                loginLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Snackbar snackbar=Snackbar.make(coordinatorLayout,"something went wrong",Snackbar.LENGTH_LONG);
                snackbar.show();
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                loginLayout.setVisibility(View.VISIBLE);

            }
        });
        

    }

    private void saveTheResponseInSharedPrefeces(LoginResponse loginResponse1) {
        SharedPref.saveStringInSharedPref(AppConstant.Token,loginResponse1.getToken(),this);
        SharedPref.saveStringInSharedPref(AppConstant.Email,loginResponse1.getUser().getEmail(),this);
        SharedPref.saveStringInSharedPref(AppConstant.Id,loginResponse1.getUser().getId(),this);
        SharedPref.saveStringInSharedPref(AppConstant.SourceApp,loginResponse1.getUser().getSourceApp(),this);
        SharedPref.saveStringInSharedPref(AppConstant.Status,loginResponse1.getUser().getStatus(),this);
        SharedPref.saveBooleanInSharedPref(AppConstant.IsLogined,true,this);


    }

    public boolean checkNetworkConnection() {
        boolean networkStatus = false;
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert conMgr != null;
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        networkStatus = netInfo != null;
        return networkStatus;
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder ab=new AlertDialog.Builder(LoginActivity.this);
        ab.setCancelable(false);
        ab.setTitle("Are you sure want to exit?");

        ab.setPositiveButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        ab.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        ab.show();
    }
}
