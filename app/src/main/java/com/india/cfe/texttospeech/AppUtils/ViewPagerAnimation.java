package com.india.cfe.texttospeech.AppUtils;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

public class ViewPagerAnimation implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.90f;
    private static final float MIN_ALPHA = 0.8f;
    @Override
    public void transformPage(@NonNull View page, float position) {

        if (position <-1){
            page.setAlpha((float) 0.8);
            }
        else if (position <=1){
            page.setScaleX(Math.max(MIN_SCALE,1-Math.abs(position)));
            page.setScaleY(Math.max(MIN_SCALE,1-Math.abs(position)));
            page.setAlpha(Math.max(MIN_ALPHA,1-Math.abs(position)));
            }
        else {
            page.setAlpha(0);
            }
            }
}
