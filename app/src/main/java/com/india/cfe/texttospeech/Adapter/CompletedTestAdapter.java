package com.india.cfe.texttospeech.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.india.cfe.texttospeech.Activity.ResultActivity;
import com.india.cfe.texttospeech.AppUtils.AppConstant;
import com.india.cfe.texttospeech.ModelClass.CompletedTestResponse;
import com.india.cfe.texttospeech.R;

import java.util.List;

public class CompletedTestAdapter extends RecyclerView.Adapter<CompletedTestAdapter.ViewHolder>{
    private Context mContext;
    private List<CompletedTestResponse> testResponses;
    private int lastPosition=-1;
    public CompletedTestAdapter(Context context, List<CompletedTestResponse> testResponse) {
        this.mContext=context;
        this.testResponses=testResponse;
    }

    @NonNull
    @Override
    public CompletedTestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.completed_test_adapter,null));
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedTestAdapter.ViewHolder holder, final int position) {
        if(testResponses.get(position).getTestName() !=null){
        holder.testNmaeTV.setText(testResponses.get(position).getTestName());
        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_from_right);
            holder.linearLayout.startAnimation(animation);
            lastPosition = position;
        }
        holder.proceedTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, ResultActivity.class);
                intent.putExtra(AppConstant.UserAnswersList,testResponses.get(position).getAnswers());
                intent.putExtra(AppConstant.TestName,testResponses.get(position).getTestName());
                intent.putExtra(AppConstant.TestID,testResponses.get(position).getTestID());
                mContext.startActivity(intent);
            }
        });

    }}

    @Override
    public int getItemCount() {
        return testResponses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView testNmaeTV,proceedTV;
        LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            testNmaeTV=itemView.findViewById(R.id.test_name);
            proceedTV=itemView.findViewById(R.id.proceed_tv);
            linearLayout=itemView.findViewById(R.id.linear_layout);
        }
    }
}
