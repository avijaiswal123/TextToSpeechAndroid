package com.india.cfe.texttospeech.AppUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitMaker {

    public Retrofit instanceMaker(String url ,String id,String auth){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(getUnsafeOkHttpClient(id,auth).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;


    }

    private static OkHttpClient.Builder getUnsafeOkHttpClient(final String id, final String auth) {
   // final String auth="JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjViNDk5OWIwYmVjYjRmMDAyMGFkZTE4OCIsImluc05hbWUiOiJQcml5YW5rYSIsImVtYWlsIjoicWFxYUBxYXFhLmNvbSIsInBhc3N3b3JkIjoiJDJhJDEwJFgxbnYwYVVYYUhNRUJ0V2tWM290NWVzWXU2VGpxREQ3cEZSMDd0VUEuQlRiT3ZFUEMybzVxIiwic291cmNlQXBwIjoiY2FuZGlkYXRlIiwidGVtcHRva2VuIjoiZmFsc2UiLCJfX3YiOjAsIkZDTXRva2VuIjoiYzc5WjY5Q2RQczg6QVBBOTFiSEhJTTZNZFItSUpyeW1XWmltS2E1cDM5clQ2bGo3akF0TFlwOEhmeU12ZXdkMmw5SUswWEhXdHdHcU83Q0ZmWEdYV1d3dVZQTm5TTm5OalhMY0t5QVY2b1JGalFlUDlsU3FEcDJ1Z1d3VzhCSTJJNUx2VTlfU3ZQQzdDZGEwd3cxYm56LVgya1NGd3daTkNENTZDb2tRTjhSTWpBIiwic3RhdGUiOiJkcmFmdCIsInN0YXR1cyI6IkFjdGl2ZSIsImFjdGl2ZSI6dHJ1ZX0sImlhdCI6MTUzMTU1MTIyNiwiZXhwIjoxNTMyMTU2MDI2fQ.DFhEGTM1pYmv8BBGHvIFmZ4nFxwZgMWNBdF0pVZgQC0";
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("id", id)
                        .addHeader("authorization",auth)
                        .build();
                return chain.proceed(request);
            }
        });
       return httpClient;
    }
}