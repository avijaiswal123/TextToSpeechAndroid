package com.india.cfe.texttospeech.ModelClass;

import java.io.Serializable;
import java.util.ArrayList;

public class CompletedTestResponse implements Serializable {

    private String _id;
    private String userID;
    private String testID;
    private String testName;
    private String __V;
    private ArrayList<String> answers;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTestID() {
        return testID;
    }

    public void setTestID(String testID) {
        this.testID = testID;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String get__V() {
        return __V;
    }

    public void set__V(String __V) {
        this.__V = __V;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }


}
