package com.india.cfe.texttospeech.Fragments.HomeScreenFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.india.cfe.texttospeech.Adapter.PracticeTestAdapter;
import com.india.cfe.texttospeech.R;

public class PracticeFragment extends Fragment {
    private RecyclerView practiceTest_recycler;
    private PracticeTestAdapter practiceTestAdapter;
    private RecyclerView.LayoutManager practiceLayoutManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.practice_fragment,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        practiceTest_recycler=view.findViewById(R.id.practice_recycler);
        practiceLayoutManager=new LinearLayoutManager(getContext());
        practiceTestAdapter=new PracticeTestAdapter(getContext());
        practiceTest_recycler.setLayoutManager(practiceLayoutManager);
        practiceTest_recycler.setAdapter(practiceTestAdapter);


    }
}
