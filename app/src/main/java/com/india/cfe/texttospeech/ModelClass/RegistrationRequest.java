package com.india.cfe.texttospeech.ModelClass;

import java.io.Serializable;

public class RegistrationRequest implements Serializable{
    private String insName;
    private String email;
    private String password;
    public String getInsName() {
        return insName;
    }

    public void setInsName(String insName) {
        this.insName = insName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }






}
